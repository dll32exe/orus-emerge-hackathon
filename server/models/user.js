var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var exp;
var db;
var schema;
var Model;

module.exports = function(_exp) {
    exp = _exp;
    var Model = mongoose.model('User' , schema);
    return Model;
};

schema = new Schema({
    _id:        ObjectId,
    user_id:    { type: String, required: true },
    name:       { type: String, required: true },
    image_url:  { type: String, required: true },
    alerts:     { type: Number, default: 0 },
    created_at: { type: Date },
    updated_at: { type: Date }
});


schema.pre('save', function(next) {
    this.updated_at = new Date();
    next();
});

schema.statics.create = function(data, callback) {
    if (!db) db = exp.server.set('db');

    var date = new Date();

    var user = new db.users({
        user_id:    data.user_id,
        name:       data.name,
        image_url:  data.image_url,
        created_at: date
    });

    user.save(function(err) {
        if (err) return callback(err);
        // var mail_options = {
        //     from:    'Topics ' + exp.secret.smtp.auth.user,
        //     to:      exp.secret.mail_to,
        //     subject: 'New User: @' + data.name,
        //     html:    "Welcome to Orus!"
        // }
        // smtp.sendMail(mail_options, sentMail)
        callback(null, user);
    });

};
