var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var exp;
var db;

module.exports = function(_exp) {
    exp = _exp;

    var Model = mongoose.model('Alert' , schema);

    Model.schema.path('category').validate(function(v) {
        return /road|plumb|police|fire|eco/i.test(v);
    });

    return Model;
};

var schema = new Schema({
    _id:          ObjectId,
    author_id:    { type: ObjectId, required: true },
    validated_by: [ObjectId],
    latitude:     { type: Number, default: 0, required: true },
    longitude:    { type: Number, default: 0, required: true },
    category:     { type: String, required: true },
    description:  { type: String, required: true },
    created_at:   { type: Date },
    updated_at:   { type: Date }
});

schema.pre('save', function(next) {
    this.updated_at = new Date();
    next();
});

schema.statics.create = function(data, callback) {
    if (!db) db = exp.server.set('db');

    var date = new Date();

    var alert = new db.users({
        author_id:   data.author_id,
        latitude:    data.latitude,
        longitude:   data.longitude,
        category:    data.category,
        description: data.description,
        created_at:  date
    });

    alert.save(function(err) {
        if (err) return callback(err);
        // var mail_options = {
        //     from:    'Topics ' + exp.secret.smtp.auth.user,
        //     to:      exp.secret.mail_to,
        //     subject: 'New User: @' + data.name,
        //     html:    "Welcome to Orus!"
        // }
        // smtp.sendMail(mail_options, sentMail)
        callback(null, alert);
    });
 
};
