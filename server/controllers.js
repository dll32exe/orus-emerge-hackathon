var path         = require("path");
var googleapis   = require('googleapis');
var OAuth2Client = googleapis.OAuth2Client;
var utils        = require('./utils');
var secret       = require("../secret");

var exp;
var db;
var controllers = {};

module.exports = function(_exp) {
    exp = _exp;
    db  = exp.set('db');
    return controllers;
};

controllers.log = function(req, res, next){
    utils.log(req, req.url);
    next();
};

controllers.index = function(req, res, next) {
    console.log(req.session.plus);
    if (req.session.plus) {
        res.sendfile(path.normalize(__dirname + "/../app.html"));
    } else {
        res.sendfile(path.normalize(__dirname + "/../index.html"));
    }
};

var oas = {};

controllers.login = function(req, res) {
    if (req.session.plus) return res.redirect("/");

    var oa = new OAuth2Client(
        secret.google.client_id,
        secret.google.client_secret,
        secret.google.callback
    );

    var oa_url = oa.generateAuthUrl({
        access_type: 'offline',
        scope: 'https://www.googleapis.com/auth/plus.me'
    });

    res.redirect(oa_url);
};

controllers.oauth2callback = function(req, res) {
    var code = req.query.code;
    if (req.session.plus) return res.redirect("/");
    if (!(code)) return utils.error(500, "oa not found", res);
    utils.log("GOT OAUTH2 CODE", code);

    var plus; // filled in gotPlus

    var oa = new OAuth2Client(
        secret.google.client_id,
        secret.google.client_secret,
        secret.google.callback
    );

    oa.getToken(code, function(err, tokens) {
        if (err) return utils.error(500, "getToken error:" + err.message, res);
        utils.log("GOT OAUTH2 TOKENS", tokens.access_token);
        oa.credentials = {
            access_token: tokens.access_token
        };
        exp.set('oa_client').plus.people
        .get({ userId: 'me' })
        .withAuthClient(oa)
        .execute(gotPlus);
    });

    function gotPlus(err, data) {
        if (err) return utils.error(500, "gotPlus error" + err.message, res);
        plus = data;
        utils.log("GOT PLUS", plus.displayName);
        db.users.findOne({ user_id : plus.id }, foundUser);
    }

    function foundUser(err, user) {
        if (err) return utils.error(500, "foundUser error" + err.message, res);
        if (user) {
            utils.log("GOT USER", user.id, user.name);
        } else {
            user = new db.users({
                user_id:   plus.id,
                name:      plus.displayName,
                image_url: plus.image.url
            });
            user.save();
        }
        req.session.plus = plus;
        req.session.user = user;
        res.redirect("/");
    }
};

controllers.alerts = function(req, res) {
};

controllers.newalert = function(req, res) {
};
